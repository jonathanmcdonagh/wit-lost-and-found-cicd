/*eslint no-unused-vars: "off" */
let express = require("express")
let path = require("path")
let favicon = require("serve-favicon")
let logger = require("morgan")
let cookieParser = require("cookie-parser")
let bodyParser = require("body-parser")

let routes = require("./routes/index")
let users = require("./routes/users")
let items = require("./routes/items.js")

let app = express()

// view engine setup
app.set("views", path.join(__dirname, "views"))
app.set("view engine", "ejs")

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, "public")))
if (process.env.NODE_ENV !== "test") {
  app.use(logger("dev"))
}
app.use("/", routes)
app.use("/users", users)

//Custom Routes
//Student Details
app.get("/student", function (req, res) {
  res.send("STUDENT DETAILS - \n" +
      "Name: Jonathan McDonagh \n" +
      "Student ID: 20074520 \n" +
      "Web App Idea: WIT Lost and Found \n" +
      "Assignment: One")
})


//(ITEMS)
//GET
app.get("/items", items.findAll) //Find all items
app.get("/items/likes", items.findTotalLikes) //Find total likes
app.get("/items/total", items.findTotalPosts) //Find total Posts
app.get("/items/:id", items.findById) //Find by ID
app.get("/items/student/:studentid", items.findByStudentId) //Find by Student ID
app.get("/rooms/:WITRoom", items.findByRoom) //Find by WIT Room
app.get("/building/:WITBuilding", items.findByBuilding) //Find by WIT Building

//POST (ITEMS)
app.post("/items",items.addItem) //Adds item
app.post("/lostitem/search", items.fuzzySearch) //Fuzzy Search

//PUT (ITEMS)
app.put("/item/:id/like", items.incrementLikes) //Adds like to item
app.put("/item/:id/update", items.updateItem) //Updates item
app.put("/item/lostitem/:id/update", items.updateLostItemName) //Updates the lost items description

//Delete (ITEMS)
app.delete("/items/:id", items.deleteItem) //Deletes item

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  let err = new Error("Not Found")
  err.status = 404
  next(err)
})

// error handlers

// development error handler
// will print stacktrace
if (app.get("env") === "development") {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500)
    res.render("error", {
      message: err.message,
      error: err
    })
  })
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500)
  res.render("error", {
    message: err.message,
    error: {}
  })
})

module.exports = app
